import socket        
import time         

s = socket.socket()        
print ("Socket successfully created")

port = 12345              

s.bind(('', port))        
print ("socket binded to %s" %(port))

s.listen(5)    
print ("socket is listening")          
 
_num = 1

c, addr = s.accept()    
while True:

    if _num == 1: c.send(str(_num).encode())
 
    data = c.recv(1024)
    if data:
        _num = int(data)
        print(_num)
        time.sleep(1)
        if _num < 100:
            _num += 1
            c.send(str(_num).encode())

    if(_num >= 100):
        print('end')
        _num = 1
        c, addr = s.accept()