import socket      
import time      
 
s = socket.socket()        
 
port = 12345               
_num = 0
s.connect(('127.0.0.1', port))

while True:
    data = s.recv(1024)
    if data:
        _num = int(data)
        print(_num)
        time.sleep(1)
        if _num < 100:
            _num += 1
            s.send(str(_num).encode())
        else:
            s.close()
    if(_num >= 100):
        s.close()
        break